import './List.css';

export default function List() {
    let planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune'];
    let planetsObjects = planets.map((value, index) => ({
        key: index,
        value: value
    }));

    return (
        <ul className="planets-list">
            {planetsObjects.map(
                (planet) => (
                    <li key={planet.key}>{planet.value}</li>
                )
            )}
        </ul>
    );
}
