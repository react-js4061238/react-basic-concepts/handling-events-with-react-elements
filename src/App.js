import './App.css';
import Title from "./Title";
import List from "./List";

import {StrictMode, useEffect, useState} from "react";

function App() {
    let [darkTheme, setDarkTheme] = useState(false);

    useEffect(() => {
        if (darkTheme)
            document.body.classList.add('dark');
        else
            document.body.classList.remove('dark')
    }, [darkTheme]);

    function changeTheme() {
        setDarkTheme(!darkTheme);
    }

    return (
        <StrictMode>
            <Title />

            <label className="switch" htmlFor="checkbox">
                <input type="checkbox" id="checkbox" />
                <div className="slider round" onClick={changeTheme} />
            </label>

            <List />
        </StrictMode>
    );
}

export default App;
